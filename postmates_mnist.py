"""
Parts of this code is taken from opensource Tensorflow authors
All rights reserved
Auhor: Sadiq Quasem
Title: Postmates Take Home Project
Description: Neural Network to detect handwritten numbers
Methods used: Tensorflow estimators and Tensorflow Serving
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import tensorflow as tf

tf.logging.set_verbosity(tf.logging.INFO)

tf.app.flags.DEFINE_integer(
    'steps', 100, 'The number of steps to train a model')
tf.app.flags.DEFINE_string(
    'model_dir', './models/ckpt/', 'Dir to save a model and checkpoints')
tf.app.flags.DEFINE_string(
    'saved_dir', './models/pb/', 'Dir to save a model for TF serving')
FLAGS = tf.app.flags.FLAGS

INPUT_FEATURE = 'image'

"""
Neural Network training pipeline for MNIST dataset
conv1: 28 x 28 x 32
BRANCH 1
    conv2_1: 14 x 14 x 64
    conv3_1: 7 x 7 x 256
BRANCH 2
    conv2_2: 14 x 14 x 64
    conv3_2: 7 x 7 x 256
merge_layer: concatanates layers 3_1 and 3_2
flatten_layer: convert to vector
fc1: fully connected layer with dropout : 1000, 0.5
fc2: fully connected layer with dropout : 500, 0.5
output: output layer used for inference
"""


def model(features, labels, mode):

    input = features[INPUT_FEATURE]

    conv1 = tf.layers.conv2d(
        inputs=input,
        filters=32,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu)

    conv1 = tf.layers.max_pooling2d(
        inputs=conv1,
        pool_size=[2, 2],
        strides=2)

    conv2_1 = tf.layers.conv2d(
        inputs=conv1,
        filters=64,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu)

    conv2_1 = tf.layers.max_pooling2d(
        inputs=conv2_1,
        pool_size=[2, 2],
        strides=2)

    conv3_1 = tf.layers.conv2d(
        inputs=conv2_1,
        filters=256,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu)

    conv2_2 = tf.layers.conv2d(
        inputs=conv1,
        filters=64,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu)

    conv2_2 = tf.layers.max_pooling2d(
        inputs=conv2_2,
        pool_size=[2, 2],
        strides=2)

    conv3_2 = tf.layers.conv2d(
        inputs=conv2_2,
        filters=256,
        kernel_size=[3, 3],
        padding="same",
        activation=tf.nn.relu)

    merge_layer = tf.concat(
        [conv3_1, conv3_2], 1)

    flatten_layer = tf.layers.flatten(merge_layer)

    fc1 = tf.layers.dense(
        inputs=flatten_layer,
        units=1000,
        activation=tf.nn.relu)

    fc1 = tf.layers.dropout(
        inputs=fc1,
        rate=0.5,
        training=mode == tf.estimator.ModeKeys.TRAIN)

    fc2 = tf.layers.dense(
        inputs=fc1,
        units=500,
        activation=tf.nn.relu)

    fc2 = tf.layers.dropout(
        inputs=fc2,
        rate=0.5,
        training=mode == tf.estimator.ModeKeys.TRAIN)

    output = tf.layers.dense(
        inputs=fc2,
        units=10)

    predictions = {
        # Generate predictions (for PREDICT and EVAL mode)
        "classes": tf.argmax(input=output, axis=1),
        # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
        # `logging_hook`.
        "probabilities": tf.nn.softmax(output, name="softmax_tensor")
    }

    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(
            mode=mode,
            predictions=predictions,
            export_outputs={
                'predict': tf.estimator.export.PredictOutput(predictions)
            })

    # Calculate Loss (for both TRAIN and EVAL modes)
    loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=output)

    # Configure the Training Op (for TRAIN mode)
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
        train_op = optimizer.minimize(
            loss=loss,
            global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

    # Add evaluation metrics (for EVAL mode)
    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(
            labels=labels, predictions=predictions["classes"])}
    return tf.estimator.EstimatorSpec(
        mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)


def serving_input_receiver_fn():
    """
    Input for Tensorflow Serving
    :return: ServingInputReceiver
    """
    receiver_tensors = {
        INPUT_FEATURE: tf.placeholder(
            tf.float32, [None, None, None, 1]),
    }

    features = {
        INPUT_FEATURE: tf.image.resize_images(
            receiver_tensors[INPUT_FEATURE], [28, 28]), }

    return tf.estimator.export.ServingInputReceiver(
        receiver_tensors=receiver_tensors, features=features)


def main(unused_argv):
    # Load training and eval data
    mnist = tf.contrib.learn.datasets.load_dataset("mnist")
    train_data = mnist.train.images  # Returns np.array
    train_labels = np.asarray(mnist.train.labels, dtype=np.int32)
    eval_data = mnist.test.images  # Returns np.array
    eval_labels = np.asarray(mnist.test.labels, dtype=np.int32)

    train_data = train_data.reshape(train_data.shape[0], 28, 28, 1)
    eval_data = eval_data.reshape(eval_data.shape[0], 28, 28, 1)

    # Create the Estimator
    training_config = tf.estimator.RunConfig(
        model_dir=FLAGS.model_dir,
        save_summary_steps=1000,
        save_checkpoints_steps=1000)
    mnist_classifier = tf.estimator.Estimator(
        model_fn=model,
        model_dir=FLAGS.model_dir,
        config=training_config)

    # Set up logging for predictions
    # Log the values in the "Softmax" tensor with label "probabilities"
    tensors_to_log = {"probabilities": "softmax_tensor"}
    logging_hook = tf.train.LoggingTensorHook(
        tensors=tensors_to_log, every_n_iter=50)

    # Train the model
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={INPUT_FEATURE: train_data},
        y=train_labels,
        batch_size=FLAGS.steps,
        num_epochs=None,
        shuffle=True)
    mnist_classifier.train(
        input_fn=train_input_fn,
        steps=5000,
        hooks=[logging_hook])

    # Evaluate the model and print results
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={INPUT_FEATURE: eval_data},
        y=eval_labels,
        num_epochs=1,
        shuffle=False)
    eval_results = mnist_classifier.evaluate(input_fn=eval_input_fn)
    print(eval_results)

    mnist_classifier.export_savedmodel(
        FLAGS.saved_dir, serving_input_receiver_fn=serving_input_receiver_fn)


if __name__ == "__main__":
    tf.app.run()
