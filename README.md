## MNIST Prediction Serving
This is an end to end Deep Learning project for the MNIST dataset, which uses models built with Tensorflow and uses Tensorflow Serving for predictions. Please note that the trained model, checkpoint files, test images have already been provided. The neural network is defined in the image below. Follow the to run and test the code.

![alt text](https://gitlab.com/sadiq.quasem/postmates-challenge/raw/develop/neural_network.png)

###### This project assumes you have the following:

```
1. Anaconda Python Distribution
2. Docker
```

After cloning the repo, please create a conda environment to make sure you have all the libraries needed to run the code. For your convenience, an `env.yml` file has been provided. Run it using:
```
conda env create -f env.yml
```
1. Train the model (or use pretrained model). You can change the steps and output paths as desired
```
python postmates_mnist.py --steps 100 --saved_dir ./models/ --model_dir ./mnist_ckpt
```
2. Build Tensorflow Serving Docker image
```
docker build --rm -f Dockerfile -t tensorflow-postmates:latest .
```
3. Run Docker container to launch Tensorflow Serving
```
docker run --rm -v ${PWD}/models:/models -e MODEL_NAME='mnist' -e MODEL_PATH='/models' -p 8500:8500  -p 8501:8501  --name tensorflow-server tensorflow-postmates:latest
```
4. Once the model is trained, you can use the sample images to perform predictions and get probabilities. The images are provided in the `/data` directory
```
python mnist_client.py --image ./data/0.png --model mnist
```

The output will look like this, where in classes, the **int64_val** = the number predicted for the image, and probabilities are shown for each of the target classes.
```
outputs {
  key: "classes"
  value {
    dtype: DT_INT64
    tensor_shape {
      dim {
        size: 1
      }
    }
    int64_val: 0
  }
}
outputs {
  key: "probabilities"
  value {
    dtype: DT_FLOAT
    tensor_shape {
      dim {
        size: 1
      }
      dim {
        size: 10
      }
    }
    float_val: 1.0
    float_val: 0.0
    float_val: 0.0
    float_val: 0.0
    float_val: 0.0
    float_val: 0.0
    float_val: 0.0
    float_val: 0.0
    float_val: 0.0
    float_val: 0.0
  }
}
model_spec {
  name: "mnist"
  version {
    value: 1537422521
  }
  signature_name: "serving_default"
}
```
